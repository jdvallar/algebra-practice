var path = require('path');
var webpack = require('webpack');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('./src')
        }
    }
};