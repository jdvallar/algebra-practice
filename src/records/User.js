import {JsonApiRecord, PropTypes} from "@/records/JsonApiRecord";

const attributes = {
    username: PropTypes.string,
    firstName: PropTypes.string,
    middleName: PropTypes.string,
    lastName: PropTypes.string,
    designation: PropTypes.string
};

export class User extends JsonApiRecord('user', attributes) {
    get displayName() {
        let name = "";
        if (this.firstName) {
            name += `${this.firstName}`;
        }

        if (this.middleName) {
            name += ` ${this.middleName}`;
        }

        if (this.lastName) {
            name += ` ${this.lastName}`;
        }

        return name.trim();
    }
}