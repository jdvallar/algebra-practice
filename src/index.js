import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {BrowserRouter, Route} from "react-router-dom";

import registerServiceWorker from './registerServiceWorker';

import store from "./store";
import Main from "./Main";

const {PUBLIC_URL} = process.env;
const basename = PUBLIC_URL.replace(/^.*\/\/[^/]+/, '');

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter basename={basename}>
            <Route component={Main} />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
