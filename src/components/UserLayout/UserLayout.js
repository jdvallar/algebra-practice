import React from "react";
import {connect} from "react-redux";
import {Route, Switch, Redirect} from "react-router-dom";
import Notifications from "react-notification-system-redux";
import {config} from "@/config";
import moment from "moment";

import Layout from 'doj-react-adminlte/Layout';
import SidebarItem from "doj-react-adminlte/Layout/SidebarItem";
import SidebarTree from "doj-react-adminlte/Layout/SidebarTree";

import UserMenu from "doj-react-adminlte/Layout/UserMenu";
import AccountSettings from "@/screens/AccountSettings";
import IntegerAddSub from "@/screens/IntegerAddSub";

class UserLayout extends React.Component {
    constructor(props) {
        super(props);
        this.userMenu = React.createRef();
    }

    handleSidebarItemClick = path => {
        // TODO: Temporary fix
        if(path !== '/') {
            path = `/${path}`;
        }

        this.props.history.push(path);
    };

    handleAccountSettingsClick = event => {
        event.preventDefault();
        this.userMenu.current.closeUserMenu();
        this.props.history.push('/account');
    };

    renderNavMenu = () => {
        const {user} = this.props.auth;
        const {enableAuthModule} = config;

        if(!enableAuthModule) return null;

        return (
            <Layout.NavMenu>
                <UserMenu firstName={user.firstName}
                          lastName={user.lastName}
                          title={user.displayName}
                          ref={this.userMenu}>
                    <UserMenu.Footer>
                        <div className="pull-left">
                            <a href={"/account"} onClick={this.handleAccountSettingsClick}
                               className="btn btn-default btn-flat">Account Settings</a>
                        </div>
                        <div className="pull-right">
                            <a href={`${process.env.PUBLIC_URL}/logout`} className="btn btn-default btn-flat">Logout</a>
                        </div>
                    </UserMenu.Footer>
                </UserMenu>
            </Layout.NavMenu>
        );
    };

    render() {
        const {notifications, auth} = this.props;
        const currentPath = this.props.location.pathname;

        const {enableAuthModule} = config;
        const {user, restrictedAccess} = auth;

        if(enableAuthModule && !user) return null;

        return (
            <React.Fragment>
                <Notifications notifications={notifications}/>
                <Layout appName={<span><b>Francesca Vallar</b> Algebra Practice</span>}
                        appNameShort={<span><b>FV</b>AP</span>}>
                    { this.renderNavMenu() }
                    <Layout.Sidebar>
                        <SidebarTree iconClass="fa fa-sort-numeric-asc" label="Integers">
                            <SidebarItem label="Addition/Subtraction" path="/add-sub"
                                         id="regular" iconClass="fa fa-plus"
                                         onClick={this.handleSidebarItemClick}
                                         active={currentPath === "/add-sub"}/>
                        </SidebarTree>
                    </Layout.Sidebar>
                    <Layout.Body>
                        <Switch>
                            <Route exact path='/account' component={AccountSettings} />
                            { restrictedAccess && <Redirect to='/account'/> }
                            <Route exact path='/integers/add-sub' component={IntegerAddSub} />
                            <Redirect to='/integers/add-sub'/>
                        </Switch>
                    </Layout.Body>
                    <Layout.Footer>
                        <strong>Copyright &copy; 2016-{moment().format("YYYY")}{" "}
                            <a href="">Justin Dane Vallar.</a>
                        </strong> All rights reserved.
                    </Layout.Footer>
                </Layout>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        notifications: state.notifications,
        auth: state.auth
    };
}

export default connect(mapStateToProps)(UserLayout);