import React from 'react';

import {TextInput} from 'doj-react-adminlte/Form'

class NumericTextInput extends React.Component {
    handleChange = (field, value) => {
        const {onChange} = this.props;

        if(!value || /^-?\d*$/g.test(value)) {
            onChange(field, value);
        }
    };

    render() {
        const {onChange, ...props} = this.props;

        return (
            <TextInput onChange={this.handleChange} {...props}/>
        );
    }
}

export default NumericTextInput;