import React from 'react';
import moment from "moment";

import Box from "doj-react-adminlte/Box";
import SelectInput from "doj-react-adminlte/Form/SelectInput";

import {LABEL_OSEC, LABEL_NPS} from "@/constants";

class SettingsBox extends React.Component {
    handleDepartmentChange = (field, value) => {
        switch(value) {
            case LABEL_OSEC: {
                // Select current OSEC payroll
                this.props.onPayrollSelect(this.props.currentOSECPayroll);
                break;
            }
            case LABEL_NPS: {
                // Select current NPS payroll
                this.props.onPayrollSelect(this.props.currentNPSPayroll);
                break;
            }
        }
    };

    handleYearChange = (field, value) => {
        const {department} = this.props;

        // Look for latest month
        switch(department) {
            case LABEL_OSEC: {
                const {osecRecords, currentOSECPayroll} = this.props;
                // Find current record.
                const current = osecRecords.find(item => item.id === currentOSECPayroll.id);

                let latestRecord;
                if(value === current.year) {
                    latestRecord = current;
                } else {
                    latestRecord = osecRecords.filter(item => item.year === value)
                        .reduce((max, cur) => (!max && cur) || (max.month > cur.month ? max : cur), null);
                }

                this.props.onPayrollSelect(latestRecord);
                break;
            }
            case LABEL_NPS: {
                const {npsRecords, currentNPSPayroll} = this.props;
                // Find current record.
                const current = npsRecords.find(item => item.id === currentNPSPayroll.id);

                let latestRecord;
                if(value === current.year) {
                    latestRecord = current;
                } else {
                    latestRecord = npsRecords.filter(item => item.year === value)
                        .reduce((max, cur) => (!max && cur) || (max.month > cur.month ? max : cur), null);
                }

                this.props.onPayrollSelect(latestRecord);
                break;
            }
        }
    };

    handleMonthChange = (field, value) => {
        const {department} = this.props;

        switch(department) {
            case LABEL_OSEC: {
                // Find OSEC record
                const record = this.props.osecRecords.find(item => item.id === value);
                this.props.onPayrollSelect(record);
                break;
            }
            case LABEL_NPS: {
                // Find NPS record
                const record = this.props.npsRecords.find(item => item.id === value);
                this.props.onPayrollSelect(record);
                break;
            }
        }
    };

    resolveYearOptions = records => {
        records = records || [];
        let options = [];

        records.forEach(item => {
            if(options.indexOf(item.year) === -1) {
                options.push(item.year);
            }
        });

        return options.map(item => ({label: item, value: item}));
    };

    resolveMonthOptions = records => {
        const {payroll} = this.props;
        const {department} = this.props;
        const year = payroll && payroll.year;
        if(!year) return [];
        records = records || [];

        let currentPayroll, currentActive;
        if(department === LABEL_OSEC) {
            currentPayroll = this.props.currentOSECPayroll;
            currentActive = this.props.currentOSECPayrollActive;
        } else if(department === LABEL_NPS) {
            currentPayroll = this.props.currentNPSPayroll;
            currentActive = this.props.currentNPSPayrollActive;
        }

        return records.filter(item => item.year === year)
            .sort((a, b) => a.month - b.month)
            .map(item => ({
                label: moment(item.month, 'MM').format('MMMM') + (
                    item.id === currentPayroll.id ?
                        (' (Current, ' + (currentActive ? 'Active' : 'Finalized') + ')' )
                        : ''
                ),
                value: item.id
            }));
    };

    resolveDepartmentOptions = () => {
        const {npsRecords, osecRecords} = this.props;

        let options = [];
        if(osecRecords && osecRecords.length) {
            options.push({value: LABEL_OSEC});
        }

        if(npsRecords && npsRecords.length) {
            options.push({value: LABEL_NPS});
        }

        return options;
    };

    getLabelFromOption = option => option.value;

    render() {
        const {department, npsRecords, osecRecords, payroll, isLoading} = this.props;
        const records = department ? (department === LABEL_OSEC ? osecRecords : npsRecords) : [];

        // TODO: Cache these.
        const departmentOptions = this.resolveDepartmentOptions();
        const yearOptions = this.resolveYearOptions(records);
        const monthOptions = this.resolveMonthOptions(records);

        return (
            <div className="row no-margin">
                <Box theme="box-primary" isLoading={isLoading}>
                    <Box.Header title="Settings"/>
                    <Box.Body>
                        <div className="pl-box-settings">
                            <div className="row">
                                <div className="col-lg-4">
                                    <SelectInput label="Department" name="department" simpleValue
                                                 onChange={this.handleDepartmentChange}
                                                 getOptionLabel={this.getLabelFromOption}
                                                 value={department} options={departmentOptions}/>
                                </div>
                                <div className="col-lg-4">
                                    <SelectInput label="Year" name="year" simpleValue
                                                 onChange={this.handleYearChange}
                                                 value={payroll && payroll.year} options={yearOptions}/>
                                </div>
                                <div className="col-lg-4">
                                    <SelectInput label="Month" name="month" simpleValue
                                                 onChange={this.handleMonthChange} options={monthOptions}
                                                 value={payroll && payroll.id}/>
                                </div>
                            </div>
                        </div>
                        <div className="pl-box-settings-action">
                            { this.props.renderActionsDropdownButton() }
                        </div>
                    </Box.Body>
                </Box>
            </div>
        );
    }
}

SettingsBox.defaultProps = {
    currentNPSPayroll: null,
    currentNPSPayrollActive: false,
    currentOSECPayroll: null,
    currentOSECPayrollActive: false,
    department: null,
    isLoading: true,
    npsRecords: [],
    onPayrollSelect: () => {},
    osecRecords: [],
    payroll: null,
    renderActionsDropdownButton: () => {}
};

export default SettingsBox;