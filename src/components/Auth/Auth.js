import React from 'react';
import {connect} from 'react-redux';
import {checkAuth} from "@/actions/authActions";

class Auth extends React.Component {
    componentDidMount() {
        this.props.dispatch(checkAuth())
            .catch(() => {
                this.props.history.replace('/login');
            });
    }

    render() {
        return null;
    }
}

function mapStateToProps({auth}) {
    return auth;
}

export default connect(mapStateToProps)(Auth);