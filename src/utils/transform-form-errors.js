export default function transformFormErrors(errorDocument) {
    let errors = {};
    errorDocument.forEach(e => {
        if(e.source && e.source.pointer) {
            const pointer = e.source.pointer;
            if(pointer.startsWith('/data/attributes/')) {
                const attribute = pointer.slice(17);
                if(!errors.hasOwnProperty(attribute)) {
                    errors[attribute] = [ e.detail ];
                } else {
                    errors[attribute].push(e.detail);
                }
            } else if(pointer.startsWith('/data/relationships/')) {
                const relationship = pointer.slice(20);
                if(!errors.hasOwnProperty(relationship)) {
                    errors[relationship] = [ e.detail ];
                } else {
                    errors[relationship].push(e.detail);
                }
            }
        }
    });

    return errors;
}
