import axios from 'axios';
import {saveAs} from 'file-saver';

import {config} from "@/config";
import {authSetAccessToken, authSetAccessFlags} from "@/actions/authActions";

export function deleteRequest(address, request = {}, axiosConfig = {}) {
    return function(dispatch, getState) {
        const { auth } = getState();

        axiosConfig = {...axiosConfig, headers: {'X-ApiKey': auth.accessToken} };

        // Insert body if request is not empty
        if(Object.keys(request).length > 0 && request.constructor === Object) {
            axiosConfig = {...axiosConfig, data: request};
        }

        return axios.delete(address, axiosConfig)
            .catch(err => {
                if(err.response) {
                    //request was made, but status code falls out of 2xx range
                    switch(err.response.status) {
                        case 401:
                            return dispatch(renewAccessToken())
                                .then(newAccessToken => {
                                    axiosConfig = { ...axiosConfig, headers: {'X-ApiKey': newAccessToken}};

                                    //resend request
                                    return axios.delete(address, axiosConfig)
                                        .catch(err => {
                                            if(err.response && err.response.status === 401) {
                                                //if accessToken fails the second time, log user out.
                                                redirectLogout();
                                            }

                                            throw err;
                                        });
                                });
                    }
                } else if(err.request) {
                    //request was made, but no response was received
                } else {
                    //an unknown error happened
                }
                throw err;
            });
    }
}

export function patchRequest(address, request, axiosConfig = {}) {
    return function(dispatch, getState) {
        const { auth } = getState();

        axiosConfig = {...axiosConfig, headers: {'X-ApiKey': auth.accessToken} };

        return axios.patch(address, request, axiosConfig)
            .catch(err => {
                if(err.response) {
                    //request was made, but status code falls out of 2xx range
                    switch(err.response.status) {
                        case 401:
                            return dispatch(renewAccessToken())
                                .then(newAccessToken => {
                                    axiosConfig = { ...axiosConfig, headers: {'X-ApiKey': newAccessToken}};

                                    //resend request
                                    return axios.patch(address, request, axiosConfig)
                                        .catch(err => {
                                            if(err.response && err.response.status === 401) {
                                                //if accessToken fails the second time, log user out.
                                                redirectLogout();
                                            }

                                            throw err;
                                        });
                                });
                    }
                } else if(err.request) {
                    // request was made, but no response was received
                } else {
                    //an unknown error happened
                }
                throw err;
            });
    };
}

export function postRequest(address, request, axiosConfig = {}) {
    return function(dispatch, getState) {
        const { auth } = getState();

        axiosConfig = {...axiosConfig, headers: {'X-ApiKey': auth.accessToken} };

        return axios.post(address, request, axiosConfig)
            .catch(err => {
                if(err.response) {
                    //request was made, but status code falls out of 2xx range
                    switch(err.response.status) {
                        case 401:
                            return dispatch(renewAccessToken())
                                .then(newAccessToken => {
                                    axiosConfig = { ...axiosConfig, headers: {'X-ApiKey': newAccessToken}};

                                    //resend request
                                    return axios.post(address, request, axiosConfig)
                                        .catch(err => {
                                            if(err.response && err.response.status === 401) {
                                                //if accessToken fails the second time, log user out.
                                                redirectLogout();
                                            }

                                            throw err;
                                        });
                                });

                    }
                } else if(err.request) {
                    //request was made, but no response was received
                } else {
                    //an unknown error happened.
                }
                throw err;
            });
    }
}

export function getFileRequest(address, filename, axiosConfig = {}) {
    return function(dispatch) {
        axiosConfig = {...axiosConfig, responseType: 'blob'};
        return dispatch(getRequest(address, axiosConfig))
            .then(response => {
                saveAs(new Blob([response.data], {
                    type: response.headers['content-type']
                }), filename);
            });
    };
}

export function postFileRequest(address, file, axiosConfig = {}) {
    return function(dispatch, getState) {
        const { auth } = getState();

        axiosConfig = {
            ...axiosConfig,
            headers: {
                'X-ApiKey': auth.accessToken,
                'Content-Type': file.type
            },
            params: {
                name: file.name
            }
        };

        return axios.post(address, file, axiosConfig)
            .catch(err => {
                if(err.response) {
                    //request was made, but status code falls out of 2xx range
                    switch(err.response.status) {
                        case 401:
                            return dispatch(renewAccessToken())
                                .then(newAccessToken => {
                                    axiosConfig = {
                                        ...axiosConfig,
                                        headers: {
                                            ...axiosConfig.headers,
                                            'X-ApiKey': newAccessToken
                                        }
                                    };

                                    //resend request
                                    return axios.post(address, file, axiosConfig)
                                        .catch(err => {
                                            if(err.response && err.response.status === 401) {
                                                //if accessToken fails the second time, log user out.
                                                redirectLogout();
                                            }

                                            throw err;
                                        });
                                });

                    }
                } else if(err.request) {
                    //request was made, but no response was received
                } else {
                    //an unknown error happened.
                }
                throw err;
            });
    }
}

export function getRequest(address, axiosConfig = {}) {
    return function(dispatch, getState) {
        const { auth } = getState();

        axiosConfig = {...axiosConfig, headers: {'X-ApiKey': auth.accessToken} };

        return axios.get(address, axiosConfig)
            .catch(err => {
                if(err.response) {
                    //request was made, but status code falls out of 2xx range
                    switch(err.response.status) {
                        case 401:
                            return dispatch(renewAccessToken())
                                .then(newAccessToken => {
                                    axiosConfig = { ...axiosConfig, headers: {'X-ApiKey': newAccessToken}};

                                    //resend request
                                    return axios.get(address, axiosConfig)
                                        .catch(err => {
                                            if(err.response && err.response.status === 401) {
                                                //if accessToken fails the second time, log user out.
                                                redirectLogout();
                                            }

                                            throw err;
                                        });
                                });
                    }
                } else if(err.request) {
                    //request was made, but no response was received
                } else {
                    //an unknown error happened.
                }
                throw err;
            });
    }
}

function renewAccessToken() {
    return function(dispatch, getState) {
        const { auth } = getState();

        return axios.post(config.apiBaseUrl + "/oauth", {
            grant_type: 'refresh_token',
            client_id: 'public',
            refresh_token: auth.refreshToken
        }, {withCredentials: true}).then(response => {
            //refresh token succeeded
            const { access_token, scope } = response.data;

            //store new access token
            dispatch(authSetAccessToken(access_token));
            dispatch(authSetAccessFlags(scope));
            return Promise.resolve(access_token);
        }).catch(err => {
            //if refresh token fails, log user out.
            redirectLogout();
            throw err;
        });
    };
}

function redirectLogout() {
    const {enableAuthModule} = config;

    if(enableAuthModule) {
        window.location = `${process.env.PUBLIC_URL}/logout`;
    }
}