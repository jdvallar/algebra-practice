export function nestReducers(flatReducers = {}, nestedReducers = {}) {
    return (state, action) => {
        let singleFlatReducer = {};

        Object.keys(flatReducers).forEach(reducerKey => {
            singleFlatReducer = {...singleFlatReducer, ...flatReducers[reducerKey](state, action)}
        });

        return Object.keys(nestedReducers).length === 0 && nestedReducers.constructor === Object ?
            singleFlatReducer : {...singleFlatReducer, ...combineNestedReducers(nestedReducers, state, action)};
    };
}

function combineNestedReducers(nestedReducers, parentState = {}, action) {
    let combinedReducers = {};

    Object.keys(nestedReducers).forEach(reducerKey => {
        combinedReducers = {
            ...combinedReducers,
            [reducerKey]: nestedReducers[reducerKey](parentState[reducerKey], action)
        }
    });

    return combinedReducers;
}