import {getRequest} from "@/utils/axios";
import {Record} from 'immutable';

export const fetchSingleResource = (endpoint, prototype, axiosConfig = {}) => {
    return dispatch => {
        // TODO: Do prototype checks here. Halt get request if prototype is not a Record or array of records

        return dispatch(getRequest(endpoint, axiosConfig))
            .then(({data: document}) => {
                const {data, included} = document;

                if (prototype.prototype instanceof Record) {
                    return new prototype(data, included);
                } else if (prototype.constructor === Array) {
                    const {type} = data;
                    for (let i = 0; i < prototype.length; i++) {
                        if (type === prototype[i].getType()) {
                            return new prototype[i](data, included);
                        }
                    }
                }
            })
            .catch(err => {
                console.log(err);
                throw err;
            });
    };
};

export const fetchResourceCollection = (endpoint, prototype, axiosConfig = {}, onSuccess = () => {}) => {
    return dispatch => {
        // TODO: Do prototype checks here. Halt get request if prototype is not a Record or array of Records

        return dispatch(getRequest(endpoint, axiosConfig))
            .then(({data: document}) => {
                const {included} = document;

                if (prototype.prototype instanceof Record) {
                    prototype = [prototype];
                }

                let collection = [];
                document.data.forEach(item => {
                    const {type} = item;
                    for (let i = 0; i < prototype.length; i++) {
                        if (type === prototype[i].getType()) {
                            collection.push(new prototype[i](item, included));
                        }
                    }
                });

                onSuccess(document);
                return collection;
            })
            .catch(err => {
                console.log(err);
                throw err;
            });
    };
};