import React from 'react';
import {connect} from 'react-redux';
import {config} from "@/config";

import {Route, Switch} from "react-router-dom";
import UserLayout from "@/components/UserLayout";
import Login from "@/screens/Login";
import {logout} from "@/actions/authActions";
import Auth from "@/components/Auth";

class Main extends React.Component {
    render() {
        const {checkedCookies} = this.props;
        const {enableAuthModule} = config;

        return (
            <Switch>
                {
                    enableAuthModule &&
                    <Route exact path='/login' component={Login}/>
                }
                {
                    enableAuthModule &&
                    <Route exact path='/logout' render={() => {this.props.dispatch(logout()); return null;}} />
                }
                {
                    enableAuthModule && !checkedCookies &&
                    <Route component={Auth}/>
                }
                <Route component={UserLayout}/>
            </Switch>
        );
    }
}

function mapStateToProps({auth}) {
    return auth;
}

export default connect(mapStateToProps)(Main);