import {combineReducers} from "redux";

import {reducer as notifications} from "react-notification-system-redux";

import auth from "./authReducer";
import accountSettings from "@/screens/AccountSettings/reducers";

export default combineReducers({
    notifications,
    accountSettings,
    auth
});