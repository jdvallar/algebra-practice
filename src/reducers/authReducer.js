import {
    AUTH_SET_ACCESS_TOKEN,
    AUTH_SET_CHECKED_COOKIES,
    AUTH_SET_LOGGED_IN,
    AUTH_SET_REFRESH_TOKEN, AUTH_SET_RESTICTED_ACCESS, AUTH_SET_ACCESS_FLAGS, AUTH_SET_USER
} from "@/actions/actionTypes";

const initialState = {
    checkedCookies: false,
    loggedIn: false,
    accessToken: null,
    refreshToken: null,
    restrictedAccess: null,
    user: null,
    hasOSECRead: false,
    hasOSECWrite: false,
    hasNPSRead: false,
    hasNPSWrite: false,
    isAdmin: false
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case AUTH_SET_CHECKED_COOKIES:
            return {...state, checkedCookies: action.payload};
        case AUTH_SET_LOGGED_IN:
            return {...state, loggedIn: action.payload};
        case AUTH_SET_ACCESS_TOKEN:
            return {...state, accessToken: action.payload};
        case AUTH_SET_REFRESH_TOKEN:
            return {...state, refreshToken: action.payload};
        case AUTH_SET_ACCESS_FLAGS: {
            const scope = action.payload;

            return {
                ...state,
                scope
            };
        }
        case AUTH_SET_RESTICTED_ACCESS:
            return {...state, restrictedAccess: action.payload};
        case AUTH_SET_USER:
            return {...state, user: action.payload};
        default:
            return state;
    }
}