import axios from "axios";
import {config} from "@/config";
import {
    AUTH_SET_ACCESS_TOKEN,
    AUTH_SET_CHECKED_COOKIES,
    AUTH_SET_LOGGED_IN,
    AUTH_SET_REFRESH_TOKEN, AUTH_SET_RESTICTED_ACCESS, AUTH_SET_ACCESS_FLAGS, AUTH_SET_USER
} from "@/actions/actionTypes";
import {getRequest} from "@/utils/axios";
import {User} from "@/records/User";

const {PUBLIC_URL} = process.env;

export function authSetLoggedIn(payload) {
    return {
        type: AUTH_SET_LOGGED_IN,
        payload
    };
}

export function authSetCheckedCookies(payload) {
    return {
        type: AUTH_SET_CHECKED_COOKIES,
        payload
    };
}

export function authSetAccessToken(payload) {
    return {
        type: AUTH_SET_ACCESS_TOKEN,
        payload
    };
}

export function authSetRefreshToken(payload) {
    return {
        type: AUTH_SET_REFRESH_TOKEN,
        payload
    };
}

export function authSetAccessFlags(payload) {
    return {
        type: AUTH_SET_ACCESS_FLAGS,
        payload
    };
}

export function authSetUser(payload) {
    return {
        type: AUTH_SET_USER,
        payload
    };
}

export function authSetRestrictedAccess(payload) {
    return {
        type: AUTH_SET_RESTICTED_ACCESS,
        payload
    };
}

export function checkAuth() {
    return function (dispatch) {
        return axios.get(`${config.apiBaseUrl}/me`, {
            withCredentials: true
        })
            .then(response => {
                const {access_token, refresh_token, restricted_access, scope} = response.data;

                dispatch(authSetAccessToken(access_token));
                dispatch(authSetRefreshToken(refresh_token));
                dispatch(authSetRestrictedAccess(restricted_access));
                dispatch(authSetAccessFlags(scope));
                dispatch(getUser())
                    .then(() => {
                        dispatch(authSetLoggedIn(true));
                        dispatch(authSetCheckedCookies(true));
                    });
            });
    }
}

export function getUser() {
    return function (dispatch) {
        return dispatch(getRequest(`${config.apiBaseUrl}/users/me`))
            .then(({data: document}) => {
                const {data} = document;

                dispatch(authSetUser(new User(data)));
            })
            .catch(err => {
                console.log(err);
                throw err;
            });
    }
}

export function login(username, password) {
    return function(dispatch) {
        return axios.post(`${config.apiBaseUrl}/oauth`, {
            grant_type: "password",
            client_id: "public",
            username,
            password
        }, {withCredentials: true})
            .then(response => {
                const {access_token, refresh_token, restricted_access, scope} = response.data;

                dispatch(authSetAccessToken(access_token));
                dispatch(authSetRefreshToken(refresh_token));
                dispatch(authSetRestrictedAccess(restricted_access));
                dispatch(authSetAccessFlags(scope));
                dispatch(getUser())
                    .then(() => {
                        dispatch(authSetLoggedIn(true));
                        dispatch(authSetCheckedCookies(true));
                    });
            });

    }
}

export function logout() {
    return function(dispatch) {
        return axios.get(`${config.apiBaseUrl}/forgetme`, {withCredentials: true})
            .then(response => {
                window.location = `${PUBLIC_URL}/login`;
            })
            .catch(err => {
                window.location = `${PUBLIC_URL}/login`;
                throw err;
            });
    }
}