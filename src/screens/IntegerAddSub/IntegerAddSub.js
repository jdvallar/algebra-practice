import React from 'react';

import Box from "doj-react-adminlte/Box";
import BreadcrumbItem from "doj-react-adminlte/Content/BreadcrumbItem";
import Content from "doj-react-adminlte/Content";
import DataTable from "doj-react-adminlte/DataTable";

import NumericTextInput from "@/components/NumericTextInput";

const columnDefs = [
    {
        header: 'Problems',
        renderValue: 'problems'
    },
    {
        header: 'Tries',
        renderValue: 'tries'
    },
    {
        header: 'Seconds per answer',
        renderValue: 'spa'
    }
];

class IntegerAddSub extends React.Component {
    constructor(props) {
        super(props);
        const timestamp = this.getTimestamp();

        this.state = {
            integers: [],
            problem: "",
            answer: "",
            errors: {},
            timestamp,
            stats: [{
                problems: 0,
                tries: -1,
                spa: 0
            }]
        }
    }

    getTimestamp = () => {
        return Math.round((new Date()).getTime() / 1000);
    };

    componentDidMount() {
        this.generateProblem();
    }

    setError = error => {
        this.setState({errors: {answer: [error]}});
    };

    generateProblem = () => {
        const integers = this.generateIntegers();
        const stats = this.state.stats[0];
        const elapsed = this.getTimestamp() - this.state.timestamp;

        this.setState({
            errors: {},
            answer: "",
            integers,
            problem: this.renderIntegers(integers),
            stats: [{
                ...stats,
                problems: stats.problems + 1,
                tries: stats.tries + 1,
                spa: (elapsed / (stats.problems + 1)).toFixed(4)
            }]
        });
    };

    generateIntegers = () => {
        const count = Math.floor(Math.random() * 4) + 2;
        let integers = [];

        for(let i = 0; i < count; i++) {
            const sign = Math.round(Math.random()) * 2 - 1;
            const abs = Math.floor(Math.random() * 20) + 1;

            integers.push(sign * abs);
        }

        return integers;
    };

    renderIntegers = integers => {
        let string = `${integers[0]}`;

        for(let i = 1; i < integers.length; i++) {
            const integer = integers[i];
            const abs = Math.abs(integer);

            if(integer > 0) {
                string += ` + ${integer}`;
            } else {
                const pars = Math.round(Math.random());
                string += pars ? ` + (${integer})` : ` - ${abs}`;
            }
        }

        return string;
    };

    handleAnswerChange = (field, value) => {
        this.setState({answer: value});
    };

    handleSubmitClick = () => {
        const {integers, answer} = this.state;
        const stats = this.state.stats[0];
        const computedSum = integers.reduce((acc, cur) => acc + cur);

        if(computedSum === parseInt(answer, 10)) {
            this.generateProblem();
        } else {
            this.setError("Incorrect.");
            this.setState({
                stats: [{
                    ...stats,
                    tries: stats.tries + 1
                }]
            });
        }
    };

    render() {
        const {problem, answer, errors, stats} = this.state;

        return (
            <Content>
                <Content.Header title="Integer Addition/Subtraction"/>
                <Content.Breadcrumb>
                    <BreadcrumbItem label="Integer Addition/Subtraction" iconClass="fa fa-plus"/>
                </Content.Breadcrumb>
                <Content.Body>
                    <div>
                        <h1>{problem}</h1>
                        <div className="row">
                            <div className="col-xs-6">
                                <NumericTextInput onEnterKey={this.handleSubmitClick}
                                                  onChange={this.handleAnswerChange} value={answer}
                                                  errors={errors} name="answer" label="Answer"/>
                            </div>
                        </div>
                        <button onClick={this.handleSubmitClick} className="btn btn-primary">Submit</button>
                    </div>
                    <div className="row" style={{marginTop: "20px"}}>
                        <div className="col-xs-6" >
                            <Box>
                                <Box.Header title="Stats"/>
                                <Box.Body>
                                    <DataTable columnDefs={columnDefs} data={stats}/>
                                </Box.Body>
                            </Box>
                        </div>
                    </div>
                </Content.Body>
            </Content>
        );
    }
}

export default IntegerAddSub;