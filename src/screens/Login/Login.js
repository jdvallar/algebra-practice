import React from 'react';
import {connect} from 'react-redux';
import Notifications from 'react-notification-system-redux';

import LoginComponent from 'doj-react-adminlte/Login';
import {authSetCheckedCookies, checkAuth, login} from "@/actions/authActions";

class Login extends React.Component {
    componentDidMount() {
        const {checkedCookies, loggedIn} = this.props;
        // If checked cookies and logged in, redirect to /
        if(loggedIn) {
            this.props.history.replace('/');
        } else if(!checkedCookies) {
            // Check if logged in
            this.props.dispatch(checkAuth())
                .then(() => {
                    this.props.history.replace('/');
                })
                .catch(() => {
                    this.props.dispatch(authSetCheckedCookies(true));
                });
        }
    }

    handleSubmit = (username, password) => {
        this.props.dispatch(Notifications.removeAll());
        this.props.dispatch(login(username, password))
            .then(() => {
                this.props.history.replace('/');
            })
            .catch(() => {
                this.props.dispatch(Notifications.error({
                    position: 'tc',
                    title: 'Failed to log-in',
                    message: 'Invalid username or password.',
                    autoDismiss: 0
                }));
            });
    };

    render() {
        const {checkedCookies, loggedIn} = this.props;
        const {notifications} = this.props;

        if(loggedIn || !checkedCookies) return null;

        return (
            <React.Fragment>
                <Notifications notifications={notifications}/>
                <LoginComponent title={<React.Fragment><strong>Payroll Administration</strong></React.Fragment>}
                                onSubmit={this.handleSubmit}
                                clearPasswordOnSubmit/>
            </React.Fragment>
        );
    }
}

function mapStateToProps({auth, notifications}) {
    return {
        ...auth,
        notifications
    };
}

export default connect(mapStateToProps)(Login);