import {
    ACCOUNT_SETTINGS_PASSWORD_FORM_CHANGE,
    ACCOUNT_SETTINGS_PASSWORD_FORM_CLEAR,
    ACCOUNT_SETTINGS_PASSWORD_FORM_SET_ERRORS
} from "@/screens/AccountSettings/actions/actionTypes";
import {postRequest} from "@/utils/axios";
import {config} from "@/config";

export function accountSettingsPasswordFormClear() {
    return {
        type: ACCOUNT_SETTINGS_PASSWORD_FORM_CLEAR,
        payload: true
    };
}

export function accountSettingsPasswordFormChange(field, value) {
    return {
        type: ACCOUNT_SETTINGS_PASSWORD_FORM_CHANGE,
        payload: {field, value}
    };
}

export function accountSettingsSetPaswordFormErrors(payload) {
    return {
        type: ACCOUNT_SETTINGS_PASSWORD_FORM_SET_ERRORS,
        payload
    };
}

export function changePassword() {
    return function(dispatch, getState) {
        const state = getState();
        const {accountSettings} = state;
        const {password, newPassword, confirmPassword} = accountSettings.accountSettings;

        const request = {
            data: {
                type: "password-change",
                attributes: {password, newPassword, confirmPassword}
            }
        };

        return dispatch(postRequest(`${config.apiBaseUrl}/users/me/password-change`, request))
            .catch(err => {
                console.log(err);
                throw err
            })
    }
}