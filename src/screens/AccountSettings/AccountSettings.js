import React from 'react';
import {connect} from 'react-redux';
import Notifications from "react-notification-system-redux";

import BreadcrumbItem from 'doj-react-adminlte/Content/BreadcrumbItem';
import Content from 'doj-react-adminlte/Content';
import Box from 'doj-react-adminlte/Box';
import Form, {TextInput} from 'doj-react-adminlte/Form';
import {
    accountSettingsPasswordFormChange, accountSettingsPasswordFormClear, accountSettingsSetPaswordFormErrors,
    changePassword
} from "@/screens/AccountSettings/actions/accountSettingsActions";
import transformFormErrors from "@/utils/transform-form-errors";

class AccountSettings extends React.Component {
    handlePasswordFormChange = (field, value) => {
        this.props.dispatch(accountSettingsPasswordFormChange(field, value));
    };

    handlePasswordFormSubmit = () => {
        this.props.dispatch(changePassword())
            .then(() => {
                this.props.dispatch(accountSettingsPasswordFormClear());
                this.props.dispatch(accountSettingsSetPaswordFormErrors({}));

                this.props.dispatch(Notifications.success({
                    title: 'Change Password',
                    message: 'Password successfully changed. You will now be redirected to the login page.'
                }));

                setTimeout(() => { window.location = `${process.env.PUBLIC_URL}/login` }, 1500);
            })
            .catch(err => {
                const errors = transformFormErrors(err.response.data.errors);
                this.props.dispatch(accountSettingsSetPaswordFormErrors(errors));

                this.props.dispatch(Notifications.error({
                    title: 'Change Password',
                    message: 'Unable to submit form. Please review the highlighted fields and try again.',
                }));
            });
    };

    render() {
        const {password, newPassword, confirmPassword} = this.props;

        return (
            <Content>
                <Content.Header title="Account Settings"/>
                <Content.Breadcrumb>
                    <BreadcrumbItem label="Account Settings" iconClass="fa fa-user" active/>
                </Content.Breadcrumb>
                <Content.Body>
                    <div className="row">
                        <div className="col-lg-offset-2 col-lg-8">
                            <Box theme="box-primary">
                                <Box.Header title="Change Password"/>
                                <Box.Body>
                                    <div className="row">
                                        <div className="col-lg-5">
                                            {
                                                this.props.restrictedAccess &&
                                                <p>
                                                    You are currently using a temporary password. Change your
                                                    password to gain access to the other parts of the site.
                                                </p>
                                            }
                                            <p>
                                                You will be logged out upon successful password change. Active
                                                sessions in other devices will also be invalidated.
                                            </p>
                                        </div>
                                        <div className="col-lg-7">
                                            <Form onChange={this.handlePasswordFormChange} errors={this.props.errors}
                                                  onEnterKey={this.handlePasswordFormSubmit}>
                                                <div className="row">
                                                    <TextInput value={password} label="Current Password" gridClass="col-xs-12"
                                                               type="password" name="password"/>
                                                    <TextInput value={newPassword} label="New Password" gridClass="col-xs-12"
                                                               type="password" name="newPassword"/>
                                                    <TextInput value={confirmPassword} label="Confirm Password" gridClass="col-xs-12"
                                                               type="password" name="confirmPassword"/>
                                                    <div className="col-xs-12">
                                                        <button type="button" onClick={this.handlePasswordFormSubmit}
                                                                className="btn btn-default">Submit</button>
                                                    </div>
                                                </div>
                                            </Form>
                                        </div>
                                    </div>
                                </Box.Body>
                            </Box>
                        </div>
                    </div>
                </Content.Body>
            </Content>
        );
    }
}

function mapStateToProps({auth, accountSettings}) {
    return {
        ...accountSettings.accountSettings,
        restrictedAccess: auth.restrictedAccess
    };
}

export default connect(mapStateToProps)(AccountSettings);