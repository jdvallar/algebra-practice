import {combineReducers} from "redux";

import accountSettings from "./accountSettingsReducer";

export default combineReducers({
    accountSettings
});