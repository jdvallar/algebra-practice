import {
    ACCOUNT_SETTINGS_PASSWORD_FORM_CHANGE,
    ACCOUNT_SETTINGS_PASSWORD_FORM_CLEAR,
    ACCOUNT_SETTINGS_PASSWORD_FORM_SET_ERRORS
} from "@/screens/AccountSettings/actions/actionTypes";

const initialState = {
    password: "",
    newPassword: "",
    confirmPassword: "",
    errors: {}
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case ACCOUNT_SETTINGS_PASSWORD_FORM_CHANGE:
            return {...state, [action.payload.field]: action.payload.value};
        case ACCOUNT_SETTINGS_PASSWORD_FORM_CLEAR:
            return {
                ...state,
                password: "",
                newPassword: "",
                confirmPassword: ""
            };
        case ACCOUNT_SETTINGS_PASSWORD_FORM_SET_ERRORS:
            return {...state, errors: action.payload};
        default:
            return state;
    }
}