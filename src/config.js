export const config = {
    apiBaseUrl: process.env.REACT_APP_API_URL,
    enableAuthModule: (process.env.REACT_APP_ENABLE_AUTH_MODULE &&
        process.env.REACT_APP_ENABLE_AUTH_MODULE === "true") || false
};